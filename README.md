# DO NOT USE IN NEW PROJECTS

This is the legacy version of Hazaar MVC that is required by some clients.

For new projects, use https://git.hazaar.io/hazaar/framework.

Please see the [documentation](https://hazaar.io) for details on how to get started with Hazaar.