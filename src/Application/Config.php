<?php
/**
 * @file        Hazaar/Application/Config.php
 *
 * @author      Jamie Carl <jamie@hazaar.io>
 *
 * @copyright   Copyright (c) 2012 Jamie Carl (http://www.hazaar.io)
 */

/**
 * @brief       The main application entry point
 */
namespace Hazaar\Application;

/**
 * @brief       Application Configuration Class
 *
 * @detail      The config class loads settings from a configuration file and configures the system ready
 *              for the application to run.  By default the config file used is application.ini and is stored
 *              in the config folder of the application path.
 *
 * @since       1.0.0
 */
class Config extends \Hazaar\Map {

    static public $override_paths = [];

    private $env;

    private $source;

    private $global;

    private $loaded = false;

    private $secure_keys = [];

    /**
     * @detail      The application configuration constructor loads the settings from the configuration file specified
     *              in the first parameter.  It will use the second parameter as the starting point and is intended to
     *              allow different operating environments to be configured from a single configuration file.
     *
     * @since       1.0.0
     *
     * @param       string  $source_file    The absolute path to the config file
     *
     * @param       string  $env            The application environment to read settings for.  Usually `development`
     *                                      or `production`.
     *
     * @param       mixed   $defaults       Initial defaut values.
     *
     * @param       mixed   $path_type      The search path type to look for configuration files.
     *
     * @param       mixed   $override_paths An array of subdirectory names to look for overrides.
     */
    function __construct($source_file = null, $env = NULL, $defaults = [], $path_type = FILE_PATH_CONFIG, $override_namespaces = false) {

        $config = null;

        if(! $env)
            $env = APPLICATION_ENV;

        $this->env = $env;

        if($source_file !== null && ($this->source = trim($source_file ?? ''))){

            if($config = $this->load($this->source, $defaults, $path_type, Config::$override_paths, $override_namespaces))
                $this->loaded = ($config->count() > 0);

        }

        $filters = [
            'out' => [
                [
                    'field' => null,
                    'callback' => [$this, 'parseString']
                ]
            ]
        ];

        parent::__construct($config, null, $filters);

    }

    public function load($source, $defaults = [], $path_type = FILE_PATH_CONFIG, $override_paths = null, $override_namespaces = false) {

        $options = [];

        $sources = [['name' => $source, 'ns' => true]];

        if($override_paths){

            if(!is_array($override_paths))
                $override_paths = [$override_paths];

            foreach($override_paths as $override)
                $sources[] = ['name' => $override . DIRECTORY_SEPARATOR . $source, 'ns' => $override_namespaces];


        }

        foreach($sources as &$source_info){

            $source_file = null;

            //If we have an extension, just use that file.
            if(ake(pathinfo($source_info['name']), 'extension', false) !== false){

                $source_file = \Hazaar\Loader::getFilePath($path_type, $source_info['name']);

            }else{ //Otherwise, search for files with supported extensions

                $extensions = ['json', 'ini']; //Ordered by preference

                foreach($extensions as $ext){

                    $filename = $source_info['name'] . '.' . $ext;

                    if($source_file = \Hazaar\Loader::getFilePath($path_type, $filename))
                        break;

                }

            }

            //If the file doesn't exist, then skip it.
            if(!$source_file) continue;

            $source_data = $this->loadSourceFile($source_file);

            $options[] = ($source_info['ns'] === true) ? $source_data : [$this->env => $this->loadSourceFile($source_file)];

        }

        if(!count($options) > 0) return false;

        $combined = [];

        foreach($options as $o){

            if(ake($combined, 'final') === true)
                break;

            $combined = array_replace_recursive($combined, $o);

        }

        $config = new \Hazaar\Map($defaults);

        if(!$this->loadConfigOptions($combined, $config, $this->env))
            return false;

        return $config;

    }

    private function loadSourceFile($source){

        $config = [];

        $cache_key = null;

        $secure_keys = [];

        //Check if APCu is available for caching and load the config file from cache if it exists.
        if(in_array('apcu', get_loaded_extensions())){

            $cache_key = md5(gethostname() . ':' . $source);

            if(apcu_exists($cache_key)) {

                $cache_info = apcu_cache_info();

                $mtime = 0;

                foreach($cache_info['cache_list'] as $cache) {

                    if(array_key_exists('info', $cache) && $cache['info'] == $cache_key) {

                        $mtime = ake($cache, 'mtime');

                        break;

                    }

                }

                if($mtime > filemtime($source)){

                    $cache_data = apcu_fetch($cache_key);

                    if(is_array($cache_data) && count($cache_data) === 2 && isset($cache_data[0]) && isset($cache_data[1]))
                        list($secure_keys, $source) = apcu_fetch($cache_key);

                }

            }

        }

        //If we have loaded this config file, continue on to the next
        if($source && !is_string($source)){

            $this->secure_keys = array_merge($this->secure_keys, $secure_keys);

            return $source;

        }

        $file = new \Hazaar\File($source);

        $extention = $file->extension();

        if($extention == 'json'){

            if(!$config = $file->parseJSON(true))
                throw new \Hazaar\Exception('Failed to parse JSON config file: ' . $source);

        }elseif($extention == 'ini'){

            if(!$config = parse_ini_string($file->get_contents(), true, INI_SCANNER_TYPED))
                throw new \Hazaar\Exception('Failed to parse INI config file: ' . $source);

            foreach($config as &$array)
                $array = array_from_dot_notation($array);

        }else{

            throw new \Hazaar\Exception('Unknown file format: ' . $source);

        }

        if($file->isEncrypted())
            $this->secure_keys = array_merge($this->secure_keys, $this->secure_keys += $secure_keys = array_keys($config));

        //Store the config file in cache
        if($cache_key !== null) apcu_store($cache_key, [$secure_keys, $config]);

        return $config;

    }

    private function loadConfigOptions($options, \Hazaar\Map $config, $env = null){

        if(!(\Hazaar\Map::is_array($options) && array_key_exists($env, $options)))
            return false;

        if(!is_array($this->global))
            $this->global = [];

        $this->global = array_merge($this->global, $options);

        foreach($options[$env] as $key => $values) {

            if($key === 'include') {

                if(!\Hazaar\Map::is_array($values))
                    $values = [$values];

                foreach($values as $include_environment)
                    $this->loadConfigOptions($options, $config, $include_environment);

            } elseif($key === 'import') {

                if(!\Hazaar\Map::is_array($values))
                    $values = [$values];

                if($import_file = current($values)){

                    do {

                        $import_env = null;

                        $import_file = $this->parseString($import_file);

                        if(strpos($import_file, ':') !== false)
                            list($import_file, $import_env) = explode(':', $import_file, 2);

                        if(!($file = \Hazaar\Loader::getFilePath(FILE_PATH_CONFIG, $import_file)))
                            continue;

                        if(is_dir($file)){

                            $dir_iterator = new \RecursiveDirectoryIterator($file);

                            $iterator = new \RecursiveIteratorIterator($dir_iterator, \RecursiveIteratorIterator::SELF_FIRST);

                            foreach($iterator as $import_file){

                                if(substr($import_file->getFileName(), 0, 1) === '.')
                                    continue;

                                array_push($values, $import_file->getRealPath() . ($import_env ? ':' . $import_env : ''));

                            }

                            continue;

                        }

                        if($options = $this->loadSourceFile($file)){
                            if(null === $import_env){
                                $options = [$env => $options];
                                $import_env = $env;
                            }
                            $this->loadConfigOptions($options, $config, $import_env);
                        }
                        
                    }while($import_file = next($values));

                }

            } else {

                $config->set($key, $values, true);

            }

        }

        return $config;

    }


    /**
     * Check whether the config was loaded from the source file.
     */
    public function loaded() {

        return $this->loaded;

    }

    /**
     * @brief       Get the config environment that was loaded
     *
     * @since       2.0.0
     */
    public function getEnv() {

        return $this->env;

    }

    public function getEnvConfig($env = null){

        if($env === null)
            $env = $this->env;

        return ake($this->global, $env);

    }

    public function getEnvironments(){

        return array_keys($this->global);

    }

    /**
     * @brief       Get the source file content from which the settings originated
     *
     * @since       1.0.0
     */
    public function getSource() {

        if(file_exists($this->source))
            return file_get_contents($this->source);

        return 'config file not found';

    }

    public function getSourceFilename() {

        return $this->source;

    }

    /**
     * Test if the current loaded source file is writable on the filesystem
     *
     * @return boolean
     */
    public function isWritable(){

        return is_writable($this->source);

    }

    /**
     * @brief       Output the configuration in a human readable format.
     *
     * @detail      This method is useful for logging, debugging or for using in application administration interfaces
     *              to check the current running configuration.
     *
     *              ### Example Output
     *
     *              <pre>
     *              app.name = Example Application
     *              app.version = 0.0.1
     *              app.layout = application
     *              app.theme.name = test
     *              app.defaultController = Index
     *              app.compress = false
     *              app.debug = 1
     *              paths.model = models
     *              paths.view = views
     *              paths.controller = controllers
     *              php.date.timezone = Australia/ACT
     *              php.display_startup_errors = 1
     *              php.display_errors = 1
     *              module.require[] = pgsql
     *              </pre>
     *
     * @since       1.0.0
     *
     * @return      string Config as a multi-line string
     */
    public function __tostring() {

        return $this->tostring();

    }

    public function tostring() {

        $config = $this->todotnotation();

        $output = "[{$this->env}]\n";

        foreach($config as $key => $value) {

            $output .= "{$key}={$value}\n";

        }

        return $output;

    }

    public function write($target = null) {

        if(!$target)
            $target = $this->source;

        if(!$target)
            return FALSE;

        $info = pathinfo($target);

        $type = ake($info, 'extension', 'json');

        $options = new \Hazaar\Map();

        //The file is a named config file ie: not an absolute file name
        if(ake($info, 'dirname') === '.' && !array_key_exists('extension', $info))
            $this->source = \Hazaar\Loader::getFilePath(FILE_PATH_CONFIG, $target . '.' . $type);

        $source = new \Hazaar\File($this->source);

        //Grab the original file so we can merge into it
        if($source->exists()){

            $source_type = $source->extension();

            if($source_type == 'json')
                $options->fromJSON($source->get_contents());

            elseif($source_type == 'ini')
                $options->fromDotNotation(parse_ini_string($source->get_contents(), TRUE, INI_SCANNER_RAW));

        }

        $options->set($this->env, $this->toArray());

        $output = '';

        if($type == 'ini'){

            foreach($options as $env => $option) {

                $output .= "[$env]" . LINE_BREAK;

                $output .= $option->todotnotation()->flatten(' = ', LINE_BREAK) . LINE_BREAK;

            }

        }else{

            $output = $options->toJSON(false, JSON_PRETTY_PRINT);

        }

        $source->set_contents($output);

        $result = $source->save();

        if($result === FALSE)
            return FALSE;

        return TRUE;

    }

    public function parseString($elem, $key = null){

        if(!(is_string($elem) && preg_match_all('/%([\w\[\]]*)%/', $elem, $matches)))
            return $elem;

        $allowed_values = [
            'GLOBALS' => $GLOBALS,
            '_SERVER' => &$_SERVER,
            '_GET' => &$_GET,
            '_POST' => &$_POST,
            '_FILES' => &$_FILES,
            '_COOKIE' => &$_COOKIE,
            '_SESSION' => &$_SESSION,
            '_REQUEST' => &$_REQUEST,
            '_ENV' => &$_ENV
        ];

        if($app = \Hazaar\Application::getInstance())
            $allowed_values['_APP'] = &$app->GLOBALS;

        foreach($matches[0] as $index => $match){

            if(strpos($matches[1][$index], '[') !== false){

                parse_str($matches[1][$index], $result);

                $parts = explode('.', key(array_to_dot_notation($result)));

                if(!array_key_exists($parts[0], $allowed_values))
                    return '';

                $value =& $allowed_values;

                foreach($parts as $part){

                    if(!($value && array_key_exists($part, $value)))
                        return '';

                    $value =& $value[$part];

                }

            }else{

                $value = defined($matches[1][$index]) ? constant($matches[1][$index]) : '';

            }

            $elem = preg_replace('/' . preg_quote($match) . '/', $value, $elem, 1);

        }

        return $elem;

    }

    public function toSecureArray(){

        $config = parent::toArray(false);

        return array_diff_key($config, array_flip($this->secure_keys));

    }

}