<?php
/**
 * @file        Hazaar/Application/Request/_Interface.php
 *
 * @author      Jamie Carl <jamie@hazaar.io>
 *
 * @copyright   Copyright (c) 2012 Jamie Carl (http://www.hazaar.io)
 */

/**
 * @brief       Application Requests Initiators.
 */
namespace Hazaar\Application\Request;

interface _Interface {

}
