<?php

/**
 * @file        Cache.php
 *
 * @author      Jamie Carl <jamie@hazaar.io>
 * @copyright   Copyright (c) 2012 Jamie Carl (http://hazaar.io)
 *
 * @version     $Id: Application.php 24593 2012-08-29 20:35:02Z jamie $
 */

namespace Hazaar;

/**
 * @brief The cache frontend
 */
class Cache implements \ArrayAccess
{
    protected $options;

    protected $use_cache = true;

    protected $backend;

    /**
     * Cache object constructor.
     *
     * @param string $backend
     *                          The name of the backend to use. Currently 'apc', 'file', 'memcached', 'session' and
     *                          'sqlite' are supported.
     * @param string $namespace
     *                          The namespace to use for grouping stored data
     *
     * @throws Cache\Exception\InvalidBackend
     * @throws Cache\Exception\InvalidFrontend
     */
    public function __construct($backend = null, $config_options = [], $namespace = 'default')
    {
        $options = new Map();

        if ($config_options) {
            $options->extend($config_options);
        }

        $looseBackend = false;

        if (!$backend) {
            // Set up a default backend chain
            $backend = ['apc', 'session'];

            // Grab the application context so we can load any cache settings
            if (($app = Application::getInstance())
                && $app->config->cache->has('backend')) {
                $backend = $app->config->cache['backend'];

                $options->extend($app->config->cache['options']);
            }

            $looseBackend = true;
        }

        $this->options = $options;

        $this->configure([
            'lifetime' => 3600,
            'use_pragma' => true,
            'keepalive' => false,
        ]);

        if ($backend instanceof Cache\Backend) {
            $this->backend = $backend;
        } else {
            if (!is_array($backend)) {
                $backend = [$backend];
            }

            // We set this now as it is an absolute safe fallback
            if (true === $looseBackend && !in_array('file', $backend)) {
                $backend[] = 'file';
            }

            foreach ($backend as $name) {
                $backendClass = '\\Hazaar\\Cache\\Backend\\'.ucfirst($name);

                if (class_exists($backendClass) && $backendClass::available()) {
                    break;
                }

                unset($backendClass);
            }

            if (!isset($backendClass)) {
                throw new Cache\Exception\NoBackendAvailable($backend);
            }

            $this->backend = new $backendClass($options, $namespace);

            if (!$this->backend instanceof Cache\Backend) {
                throw new Cache\Exception\InvalidBackend($backendClass);
            }
        }

        /*
         * Cache skip
         *
         * Check for a Pragma header to see if we should skip loading from cache.
         */
        if ($this->options->use_pragma && function_exists('apache_request_headers')) {
            $headers = apache_request_headers();

            if (array_key_exists('Pragma', $headers) && 'no-cache' == $headers['Pragma']) {
                $this->use_cache = false;
            }
        }
    }

    public function can($capability)
    {
        return $this->backend->can($capability);
    }

    public function configure($options)
    {
        $this->options->enhance($options);
    }

    public function getBackend()
    {
        return $this->backend;
    }

    public function getBackendName()
    {
        return $this->backend->getName();
    }

    public function setBackendOption($key, $value)
    {
        $this->backend->options->extend([$key => $value]);
    }

    /**
     * Retrieve a value from cache storage.
     *
     * @param mixed $key
     *                            The reference key used to store the value
     * @param bool  $default
     *                            If the value doesn't exist, this default will be returned instead
     * @param bool  $save_default
     *                            If the value doesn't exist and a default is specified, save that default to cache
     *
     * @return mixed the value that was stored in cache
     */
    public function &get($key, $default = false, $save_default = false)
    {
        if (!$this->use_cache) {
            return $default;
        }

        $result = $this->backend->get($key);

        if ($result && !$this->backend->can('store_objects')) {
            $result = unserialize($result);
        }

        if (false === $result) {
            if (true === $save_default) {
                $this->set($key, $default);
            }

            return $default;
        }

        return $result;
    }

    /**
     * Store a value in the cache using the current cache backend.
     *
     * @param mixed $key
     *                       The reference key under which to store the value
     * @param mixed $value
     *                       The value that should be stored. Values can be pretty much anything including integers, booleans,
     *                       strings and any object that can be serialised.
     * @param mixed $timeout
     *                       The number of seconds after which the value becomes invalid. If not set the global
     *                       'lifetime' option is used. Set a value of '-1' to indicate that the value should never timeout.
     *
     * @return bool boolean indicating whether or not the operation succeeded
     */
    public function set($key, $value, $timeout = null)
    {
        /*
         * If the backend can't store objects, serialize the value.
         */
        if (!$this->backend->can('store_objects')) {
            $value = serialize($value);
        }

        return $this->backend->set($key, $value, $timeout);
    }

    /**
     * Check if a stored value exists.
     *
     * @param mixed $key
     *                           The value key to check for
     * @param bool  $check_empty Normally this method will return try if the value exists with `$key`.  Setting `$check_empty` looks at the value
     *                           and will return false if it is an 'empty' value (ie: 0, null, [])
     *
     * @return bool returns TRUE or FALSE indicating if the value is stored
     */
    public function has($key, $check_empty = false)
    {
        return $this->backend->has($key, $check_empty);
    }

    /**
     * Removes a stored value.
     *
     * @param mixed $key
     *                   The key of the value to remove
     *
     * @return bool tRUE indicates the value existed and was removed. FALSE otherwise.
     */
    public function remove($key)
    {
        return $this->backend->remove($key);
    }

    public function extend($array, $recursive = false)
    {
        if (!is_array($array)) {
            return false;
        }

        foreach ($array as $key => $value) {
            if (is_array($value) && $recursive) {
                $c = $this->get($key);

                if ($c && is_array($c)) {
                    $value = array_merge_recursive($c, $value);
                }

                $this->set($key, $value);
            } else {
                $this->set($key, $value);
            }
        }

        return true;
    }

    public function clear()
    {
        return $this->backend->clear();
    }

    public function setValues($values)
    {
        if (!is_array($values)) {
            return false;
        }

        foreach ($values as $key => $value) {
            $this->set($key, $value);
        }

        return true;
    }

    public function pull($key)
    {
        $value = $this->get($key);

        $this->remove($key);

        return $value;
    }

    public function toArray()
    {
        if (!$this->backend->can('array')) {
            return false;
        }

        $values = $this->backend->toArray();

        if ($values && !$this->backend->can('store_objects')) {
            foreach ($values as &$value) {
                $value = unserialize($value);
            }
        }

        return $values;
    }

    public function all()
    {
        if (!$this->backend->can('all')) {
            return false;
        }

        return $this->backend->all();
    }

    public function kill($namespace, &$data = null)
    {
        if (!$this->backend->can('all')) {
            return false;
        }

        return $this->backend->kill($namespace, $data);
    }

    /**
     * Disable the use of cache.
     *
     * This allows a cache object to be disabled but still be used without having to modify existing application
     * code.  The application can still call the get() method but it will always return false, simulating a negative
     * cache response.  Calling set() will still have an affect however.
     */
    public function on()
    {
        $this->use_cache = true;
    }

    /**
     * Enable the use of cache.
     *
     * Cache is enabled by default.  This is to allow cache to be re-enabled after calling off().
     *
     * This method can also be used to force cache back on after being disabled by a "Pragma: no-cache" header
     * in the case where the use_pragma setting is enabled (which is the default).
     */
    public function off()
    {
        $this->use_cache = false;
    }

    /*
     * MAGIC METHOD FOR DIRECT ACCESS
     */
    public function __isset($key)
    {
        return $this->has($key);
    }

    public function &__get($key)
    {
        return $this->get($key);
    }

    public function __set($key, $value)
    {
        return $this->set($key, $value);
    }

    public function __unset($key)
    {
        return $this->remove($key);
    }

    /*
     * ARRAYACCESS METHODS
     */
    public function offsetExists($offset): bool
    {
        return $this->has($offset);
    }

    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }

    public function offsetUnset($offset): void
    {
        $this->remove($offset);
    }

    /**
     * Increment key value.
     *
     * This method will increment a cached integer value by a defined amount (default is 1).  Once
     * the value is incremented it will be stored back in the cache and the new value returned.
     *
     * @param string $key    The cache key
     * @param int    $amount the amount to increment the value by
     *
     * @return int The new incremented value
     */
    public function increment($key, $amount = 1)
    {
        if (($value = $this->get($key)) === null || !is_int($value)) {
            $value = 0;
        }

        if (!is_int($amount)) {
            $amount = 1;
        }

        $this->set($key, $value += $amount);

        return $value;
    }

    /**
     * Decrement key value.
     *
     * This method will decrement a cached integer value by a defined amount (default is 1).  Once
     * the value is decremented it will be stored back in the cache and the new value returned.
     *
     * @param string $key    The cache key
     * @param int    $amount the amount to decrement the value by
     *
     * @return int The new decremented value
     */
    public function decrement($key, $amount = 1)
    {
        if (($value = $this->get($key)) === null || !is_int($value)) {
            $value = 0;
        }

        if (!is_int($amount)) {
            $amount = 1;
        }

        $this->set($key, $value -= $amount);

        return $value;
    }

    public function lock($key)
    {
        return $this->backend->lock($key);
    }

    public function unlock($key)
    {
        return $this->backend->unlock($key);
    }
}
