<?php

namespace Hazaar\Model;

/**
 * DataBinderValue short summary.
 *
 * DataBinderValue description.
 *
 * @version 1.0
 * @author jamie
 */
class DataBinderValue implements \JsonSerializable, \ArrayAccess {

    public $name;

    public $value;

    public $label;

    public $other;

    public $orgValue;

    function __construct($value, $label = null, $other = null){

        \call_user_func_array([$this, 'set'], func_get_args());

    }

    /**
     * Get a new DataBinderValue object if the supplied value is valid and non-null
     *
     * If the value is NULL, then a new DataBinderValue will not be returned.  This method is useful for
     * executing code conditionally if the value is a valid DataBinderValue value.
     *
     * @param mixed $value
     * @param mixed $label
     * @return DataBinderValue|null
     */
    static function create($value, $label = null, $other = null){

        if($value === null)
            return null;

        return new DataBinderValue($value, $label, $other);

    }

    public function __toString(){

        if($this->name)
            return (string)new \Hazaar\Html\Span(coalesce($this->label, $this->value), ['data-bind' => $this->name]);

        if(!$this->value && $this->other)
            return $this->other;

        return (string)coalesce($this->label, $this->value);

    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize() {

        return $this->toArray();

    }

    public function toArray(){

        if(!($this->label || $this->other || $this->orgValue)) return $this->value;

        $array = ['__hz_value' => $this->value];
        
        if($this->label) $array['__hz_label'] = $this->label;

        if($this->other) $array['__hz_other'] = $this->other;

        if($this->orgValue) $array['__hz_org_value'] = $this->orgValue;

        return $array;

    }

    /**
     * Resolve an array and look for saved value/label arrays and convert them.
     *
     * @param mixed $object The object/array to resolve
     * @param boolean $recursive Recurse into normal arrays.  Defaults to TRUE.
     * @return array
     */
    static function resolve($object, $recursive = true){

        $array = [];

        if(is_array($object) || $object instanceof \stdClass){

            foreach($object as $key => $value){

                if(is_array($value) || $value instanceof \stdClass){

                    if((is_array($value) && array_key_exists('__hz_value', $value)) || ($value instanceof \stdClass && property_exists($value, '__hz_value')))
                        $value = (DataBinderValue::create($value))->value;
                    elseif($recursive === true)
                        $value = DataBinderValue::resolve($value);

                }

                $array[$key] = $value;

            }

        }

        if(count($array) === 0)
            return null;

        return $array;

    }

    public function export(){

        return coalesce($this->value, $this->other, $this->label);

    }

    public function set($value, $label = null, $other = null){

        $orgValue = null;

        if(is_array($value)){

            if(array_key_exists('__hz_org_value', $value))
                $orgValue = $value['__hz_org_value'];

            if(array_key_exists('__hz_other', $value))
                $other = $value['__hz_other'];

            if(array_key_exists('__hz_label', $value))
                $label = $value['__hz_label'];

            if(array_key_exists('__hz_value', $value))
                $value = $value['__hz_value'];

        }elseif($value instanceof \stdClass){

            if(property_exists($value, '__hz_org_value'))
                $orgValue = $value->__hz_org_value;

            if(property_exists($value, '__hz_other'))
                $other = $value->__hz_other;

            if(property_exists($value, '__hz_label'))
                $label = $value->__hz_label;

            if(property_exists($value, '__hz_value'))
                $value = $value->__hz_value;

        }

        $this->value = $value;

        $this->label = $label;

        $this->other = $other;

        $this->orgValue = $orgValue;

    }

    public function offsetExists($offset){

        return property_exists($this, $offset);

    }

    public function offsetGet($offset){

        return $this->$offset;

    }

    public function offsetSet($offset, $value){

        return;

    }

    public function offsetUnset($offset){

        return;

    }

}