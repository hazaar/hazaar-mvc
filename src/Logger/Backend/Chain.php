<?php

namespace Hazaar\Logger\Backend;

class Chain extends \Hazaar\Logger\Backend {

    private $backends = [];

    public function init() {

        $this->setDefaultOption('chain', ['backend' => ['file']]);

        $chain = $this->getOption('chain');

        if(is_array($chain['backend'])) {

            foreach($chain['backend'] as $backend_name) {

                $backend_class = '\Hazaar\\Logger\\Backend\\' . ucfirst($backend_name);

                $backend = new $backend_class([]);

                $this->backends[] = $backend;

                foreach($backend->getCapabilities() as $capability)
                    $this->addCapability($capability);

            }

        }

    }

    public function postRun() {

        foreach($this->backends as $backend) {

            $backend->postRun();

        }

    }

    public function write($tag, $message, $level = LOG_NOTICE, $request = null) {

        foreach($this->backends as $backend) {

            if(!$backend->can('write_objects') && (is_array($message) || is_object($message))) {

                $backend->write($tag, preg_replace('/\s+/', ' ', print_r($message, true)), $level, $request);

            } else {

                $backend->write($tag, $message, $level, $request);

            }

        }

    }

    public function trace() {

        foreach($this->backends as $backend) {

            if($backend->can('write_trace'))
                $backend->trace();

        }

    }

}
